﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Mainmenu : MonoBehaviour {


    private const float CAMERA_TRANSMISION_SPEED = 5.00f;
    public GameObject LevelButtonPrefab;

    public GameObject LevelButtonContanier;

    private Transform cameraTransform;

    private Transform cameraDesiredLookAt;

    private void Start()
    {
        cameraTransform = Camera.main.transform;
        Sprite[] thumbnails = Resources.LoadAll<Sprite>("Levels");
        foreach (Sprite thumbnail in thumbnails)
        {
            GameObject contanier = Instantiate(LevelButtonPrefab) as GameObject;
            contanier.GetComponent<Image>().sprite = thumbnail;
            contanier.transform.SetParent(LevelButtonContanier.transform, false);

            string SceneName = thumbnail.name;
            contanier.GetComponent<Button>().onClick.AddListener(() => loadLevel(SceneName));
        }
    }
    private void Update()
    {
        if (cameraDesiredLookAt != null)
        {
            cameraTransform.rotation = Quaternion.Slerp(cameraTransform.rotation, cameraDesiredLookAt.rotation, CAMERA_TRANSMISION_SPEED * Time.deltaTime);
        }
    }
    private void loadLevel(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void lookAtMenu(Transform MenuTransform)
    {
        loadLevel("mainGame");
       // cameraDesiredLookAt = MenuTransform;
    }

}
