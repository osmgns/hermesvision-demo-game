﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombMove : MonoBehaviour {

    public float fpsTargetDistance;
    public float enemyLookDistance;
    public float speed;
    public float damping;   
    public Transform fpsTarget;
    Rigidbody theRigidbody;
    Renderer myRenderer;
    // Animator anim;


    // Use this for initialization
    void Start()
    {
        myRenderer = GetComponent<Renderer>();
        theRigidbody = GetComponent<Rigidbody>();

  
        //  anim = GetComponent<Animator>();

    }

    void FixedUpdate()
    {
        fpsTargetDistance = Vector3.Distance(fpsTarget.position, transform.position);
        if (fpsTargetDistance < enemyLookDistance)
        {
            lookAtPlayer();
        }
       


        followPlayer();

    }

    void lookAtPlayer()
    {

        Quaternion rotation = Quaternion.LookRotation(fpsTarget.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
    }

    void followPlayer()
    {

        //  Quaternion rotation = Quaternion.LookRotation(fpsTarget.position - transform.position);
        //   transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
        transform.position = Vector3.MoveTowards(transform.position, fpsTarget.position, speed * Time.deltaTime);
    }
    
}
