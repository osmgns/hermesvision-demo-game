﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject enemies;
    public Vector3 respawnValue;
    public float respawnWait;
    public float respawnMostWait;
    public float respawnLeastWait;
    public int startWait;
    public int bombcount;

    int randEnemy;

	// Use this for initialization
	void Start () {
        StartCoroutine(waitSpawner());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator waitSpawner()
    {
        yield return new WaitForSeconds(startWait);

        while (bombcount<=5)
        {
           
            Vector3 respawnPosition = new Vector3(Random.Range(-respawnValue.x,respawnValue.x),  respawnValue.y, Random.Range(-respawnValue.z, respawnValue.z));

            Instantiate(enemies, respawnPosition + transform.TransformPoint(0, 0, 0), gameObject.transform.rotation);
            yield return new WaitForSeconds(respawnWait);
            bombcount += 1;
        }

    }
}
