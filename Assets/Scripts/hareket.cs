﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hareket : MonoBehaviour {
    float harekethızı= 15f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector3 hareket = new Vector3(Input.acceleration.x * harekethızı, 0, Input.acceleration.y * harekethızı);
        Vector3 yenikonum = gameObject.GetComponent<Rigidbody>().position + hareket*Time.fixedDeltaTime;
        gameObject.GetComponent<Rigidbody>().MovePosition(yenikonum);
    }
}
