﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class initiatefire : MonoBehaviour {

    // Use this for initialization
    public GameObject prefab;
    public float startOil = 300;
    public float CurrentOil;
    public float oilamount = 1;
    public bool flag;
    public Image oilBar;
    private void Start()
    {
        CurrentOil = startOil;
        prefab.SetActive(false);
        flag = false;
    }
    private void FixedUpdate()
    {
        if (flag==true)
        {
            CurrentOil -= oilamount;
            oilBar.fillAmount = CurrentOil / startOil;
        }
    }
    public   void firethrow()
    {
        if (CurrentOil>0)
        {
            flag = true;
        
        prefab.SetActive(true);
        }

        //obj = Instantiate(prefab, prefab.transform.position, prefab.transform.rotation) as GameObject;
        //obj.transform.parent = transform;
        //obj.transform.localPosition = new Vector3(prefab.transform.position.x, prefab.transform.position.y, prefab.transform.position.z);
    }

    public void finishfire()
    {
        flag = false;

        prefab.SetActive(false);
    }
}
