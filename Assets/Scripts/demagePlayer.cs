﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class demagePlayer : MonoBehaviour {

    public float playerStartHealth = 5000;
    public float playerCurrntHealth;
    float demage = 100;
    public GameObject prefab;
    [Header("Unity stuf")]
    public Image healthBar;
    void Start()
    {
       
        prefab.SetActive(false);
        playerCurrntHealth = playerStartHealth;
       
    }


    void OnCollisionEnter(Collision _collision)
    {
        if (_collision.gameObject.tag == "Player")
        {
            playerCurrntHealth -= demage;
            healthBar.fillAmount = playerCurrntHealth/ playerStartHealth;
            if (playerCurrntHealth<=0)
            {
                SceneManager.LoadScene("gameover");
            }
        //    prefab.SetActive(true);
        }
  
      //  else
           // prefab.SetActive(false);
    }
    private void OnCollisionStay(Collision _collision)
    {
        if (_collision.gameObject.tag == "Player")
        {
            playerCurrntHealth -= 10;
            healthBar.fillAmount = playerCurrntHealth / playerStartHealth;
            // prefab.SetActive(true);
            if (playerCurrntHealth <= 0)
            {
                SceneManager.LoadScene("gameover");
            }
        }

        // else
        //  prefab.SetActive(false);
    }
    private void OnTriggerStay(Collider _collision)
    {
        if (_collision.gameObject.tag == "Player")
        {
            playerCurrntHealth -= 10;
            healthBar.fillAmount = playerCurrntHealth / playerStartHealth;
            // prefab.SetActive(true);
            if (playerCurrntHealth <= 0)
            {
                SceneManager.LoadScene("gameover");
            }
        }

       // else
          //  prefab.SetActive(false);
    }
  



}