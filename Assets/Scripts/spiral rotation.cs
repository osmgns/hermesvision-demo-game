﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spiralRotation : MonoBehaviour {

    public bool rotationdirection;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (rotationdirection)
        {
            transform.Rotate(new Vector3(Time.deltaTime * 700, 0, 0));
        }
        else
            transform.Rotate(new Vector3(-Time.deltaTime * 700, 0, 0));
    }
}
